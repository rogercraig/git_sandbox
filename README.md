# GIT Play Ground

## 1. Intro...
This is a git playground for messing up and trying to recover, etc...

## 2. Resolve a conflict between Forks
Let's say this repository:
```
https://perenglundus@bitbucket.org/idgwebdevelopers/git_sandbox.git
```
```
And my staging area:
https://perenglundus@bitbucket.org/perenglundus/git_sandbox.git
```
## 3. Roger's
Then Roger makes a change and push and merge into this repo, as below:
This is Roger just adding a line or 2 to Per's README.md
My new fork is git clone 
```
https://rogercraig@bitbucket.org/rogercraig/git_sandbox.git
```

## 4. To Resolve
```
git pull https://perenglundus@bitbucket.org/idgwebdevelopers/git_sandbox.git

// Resolve conflicts

git add README.md

git commit -m "My changes merged with Roger's"

git push https://perenglundus@bitbucket.org/perenglundus/git_sandbox.git

```
When you check the pull request, the pull request will still be open and you'll no longer see any merge conflicts.